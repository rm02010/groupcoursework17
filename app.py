from flask import Flask, request, jsonify
import joblib
import logging
from datetime import datetime

app = Flask(__name__)

# Load the trained model, TF-IDF vectorizer, and MultiLabelBinarizer
model = joblib.load('group_coursework_group17_model.pkl')
tfidf_vectorizer = joblib.load('group_coursework_group17_tfidf_vectorizer.pkl')
mlb = joblib.load('group_coursework_group17_mlb.pkl')

# Configure logging
logging.basicConfig(filename='prediction_logs.txt', level=logging.INFO)

@app.route('/predict', methods=['POST'])
def predict():
    data = request.json
    tokens = data['tokens']
    tokens_str = ' '.join(tokens)
    tokens_tfidf = tfidf_vectorizer.transform([tokens_str])
    prediction = model.predict(tokens_tfidf)
    prediction_labels = mlb.inverse_transform(prediction)
    
    # Log the input and output
    logging.info(f"{datetime.now()} - Tokens: {tokens} - Predicted NER Tags: {prediction_labels[0]}")
    
    return jsonify({'predicted_ner_tags': prediction_labels[0]})

if __name__ == '__main__':
    app.run(debug=True, use_reloader=False)

