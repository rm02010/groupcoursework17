# train_model.py

# Import necessary libraries
from datasets import load_dataset
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.tree import DecisionTreeClassifier
from sklearn.multioutput import MultiOutputClassifier
from sklearn.preprocessing import MultiLabelBinarizer
import joblib

# Load dataset
dataset = load_dataset('surrey-nlp/PLOD-CW')
train_data = dataset['train']
validation_data = dataset['validation']
test_data = dataset['test']

# Preprocess the data
def preprocess_data(data):
    tokens = [' '.join(instance['tokens']) for instance in data]
    ner_tags = [instance['ner_tags'] for instance in data]
    return tokens, ner_tags

train_tokens, train_ner_tags = preprocess_data(train_data)
validate_tokens, validate_ner_tags = preprocess_data(validation_data)
test_tokens, test_ner_tags = preprocess_data(test_data)

mlb = MultiLabelBinarizer()
y_train = mlb.fit_transform(train_ner_tags)
y_validate = mlb.transform(validate_ner_tags)
y_test = mlb.transform(test_ner_tags)

# Vectorize tokens using TF-IDF
tfidf_vectorizer = TfidfVectorizer()
X_train = tfidf_vectorizer.fit_transform(train_tokens)
X_validate = tfidf_vectorizer.transform(validate_tokens)
X_test = tfidf_vectorizer.transform(test_tokens)

# Train the model
decision_tree = DecisionTreeClassifier()
classifier = MultiOutputClassifier(decision_tree, n_jobs=-1)
classifier.fit(X_train, y_train)

# Save the model, vectorizer, and MultiLabelBinarizer
joblib.dump(classifier, 'group_coursework_group17_model.pkl')
joblib.dump(tfidf_vectorizer, 'group_coursework_group17_tfidf_vectorizer.pkl')
joblib.dump(mlb, 'group_coursework_group17_mlb.pkl')

print("Model training and saving completed successfully.")

